package com.xueliman.iov.auth_server_resource_common.other;

import java.util.Arrays;

/**
 * @author zxg
 */
public enum ResultCode implements IResultCode {
    SUCCESS(true, 200, "操作成功"),
    FAILURE(false, 500, "操作失败");

    boolean success;
    int code;
    String message;

    private ResultCode(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public static ResultCode getResultCodeByCode(int code) {
        return (ResultCode) Arrays.stream(values()).filter((s) -> {
            return s.code == code;
        }).findFirst().get();
    }

    public boolean success() {
        return this.success;
    }

    public int code() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
